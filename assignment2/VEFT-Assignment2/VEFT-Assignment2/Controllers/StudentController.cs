﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services;

namespace VEFT_Assignment2.Controllers
{
    /// <summary>
    /// Controller for Students
    /// </summary>
    [RoutePrefix("api/courses/{courseId}/students")]
    public class StudentController : ApiController
    {
        private readonly StudentServiceProvider _service;

        /// <summary>
        /// Initializes a student service provider which contains all business logic
        /// </summary>
        public StudentController()
        {
            _service = new StudentServiceProvider();
        }

        /// <summary>
        /// Lists all students registered in given course
        /// </summary>
        /// <param name="courseId">Id of the course</param>
        /// <returns>List of students in course</returns>
        [HttpGet]
        [Route("")]
        public List<StudentDTO> GetStudents(int courseId)
        {
            return _service.GetStudentsByCourse(courseId);
        }

        /// <summary>
        /// Registers a new student to the given course
        /// </summary>
        /// <param name="courseId">Id of the course</param>
        /// <param name="model">Data for the new student</param>
        /// <returns>Object containing data of the newly added student</returns>
        [HttpPost]
        [Route("")]
        public StudentDTO PostStudent(int courseId, StudentViewModel model)
        {
            return _service.PostStudentToCourse(courseId, model);
        }
    }
}
