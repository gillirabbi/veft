﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services.Entities;
using API.Services.Repositories;

namespace API.Services
{
    /// <summary>
    /// Service provider class for Students
    /// </summary>
    public class StudentServiceProvider
    {
        private readonly AppDataContext _db;

        /// <summary>
        /// Initializes database context when created
        /// </summary>
        public StudentServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// Gets all students registered in the given course
        /// </summary>
        /// <param name="courseId">Id of course being fetched from</param>
        /// <returns>A list of students in the given course</returns>
        public List<StudentDTO> GetStudentsByCourse(int courseId)
        {
            var result = (from s in _db.Students
                from cs in _db.CourseStudents
                where s.Id == cs.StudentId
                where cs.CourseId == courseId
                select new StudentDTO
                {
                    Name = s.Name,
                    SSN = s.SSN
                }).ToList();
            return result;
        }

        /// <summary>
        /// Adds a new student to the given course
        /// </summary>
        /// <param name="courseId">Id of course being added to</param>
        /// <param name="model">Viewmodel with the new students info</param>
        /// <returns>An object with the new students data</returns>
        public StudentDTO PostStudentToCourse(int courseId, StudentViewModel model)
        {
            var student = new Student { Name = model.Name, SSN = model.SSN };
            _db.Students.Add(student);
            _db.SaveChanges();
            _db.CourseStudents.Add(new CourseStudent { CourseId = courseId, StudentId = student.Id });
            _db.SaveChanges();

            return new StudentDTO { Name = model.Name, SSN = model.SSN };
        }
    }
}
