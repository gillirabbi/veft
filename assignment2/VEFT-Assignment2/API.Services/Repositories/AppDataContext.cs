﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Services.Entities;

namespace API.Services.Repositories
{
    /// <summary>
    /// A DB context for linking entity classes to the database
    /// Each entity is linked with it's data base table counterpart
    /// Example:
    ///     Entity: Course
    ///     Table: Courses
    /// </summary>
    class AppDataContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseTemplate> CourseTemplates { get; set; }
        public DbSet<CourseStudent> CourseStudents { get; set; }
        public DbSet<Student> Students { get; set; }

    }
}
