﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents a relation between a course in a university and it's registered students
    /// It offers a many-to-many relation
    /// </summary>
    class CourseStudent
    {
        /// <summary>
        /// The id of the course-student relation
        /// This id is never used, but kept as a unique id for the relation
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The id of the course in the relation
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// The id of the student in the relation
        /// </summary>
        public int StudentId { get; set; }
    }
}
