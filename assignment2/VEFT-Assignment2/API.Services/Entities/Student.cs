﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents a student in a university
    /// </summary>
    class Student
    {
        /// <summary>
        /// The unique id of the student
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the student
        /// Example: Jón Jónsson
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The social security number of the student
        /// Example: 0202891239
        /// </summary>
        public string SSN { get; set; }
    }
}
