﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents a course in a university
    /// </summary>
    class Course
    {
        /// <summary>
        /// Unique ID for a given course
        /// Example: 1
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ID code for a given course
        /// Example: "T-514-VEFT"
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// Start date of a given course
        /// Example: 2015-08-17
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of a given course
        /// Example: 2015-11-08
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The semester in which the course is taught
        /// Example: "20153"
        /// </summary>
        public string Semester { get; set; }
    }
}
