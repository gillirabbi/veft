﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Services.Entities
{
    /// <summary>
    /// This class ties a courses TemplateId to the course name
    /// </summary>
    class CourseTemplate
    {
        /// <summary>
        /// Unique id for the relation
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The template id of the course
        /// Example: T-514-VEFT
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// The name of the course
        /// Example: Vefþjónustur
        /// </summary>
        public string Name { get; set; }
    }
}
