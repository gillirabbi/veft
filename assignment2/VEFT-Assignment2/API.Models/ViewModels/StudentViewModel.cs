﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.ViewModels
{
    /// <summary>
    /// This class represents a new student being registered
    /// </summary>
    public class StudentViewModel
    {
        /// <summary>
        /// Name of the given student
        /// Example: Ingvi Andrason
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The social security number of the given student
        /// Example: 0102901239
        /// </summary>
        public string SSN { get; set; }
    }
}
