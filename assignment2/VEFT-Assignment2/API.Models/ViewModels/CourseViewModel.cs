﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.ViewModels
{
    /// <summary>
    /// This class represents a course being created or edited
    /// </summary>
    public class CourseViewModel
    {
        /// <summary>
        /// The template id of given course
        /// Example: T-514-VEFT
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// The date that the given course starts
        /// Example: 2015-08-08
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The date that hte given course ends
        /// Example: 2015-11-11
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The semester inwhich the given course is being taught
        /// Example: 20153
        /// </summary>
        public string Semester { get; set; }
    }
}
