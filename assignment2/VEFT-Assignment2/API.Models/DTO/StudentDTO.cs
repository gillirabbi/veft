namespace API.Models.DTO
{
    /// <summary>
    /// This class represents an item in a list of students
    /// </summary>
    public class StudentDTO
    {
        /// <summary>
        /// The name of the student
        /// Example: "Gunnar Gylfason"
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The social security number for a given student
        /// Example: "1407892779"
        /// </summary>
        public string SSN { get; set; }
    }
}
