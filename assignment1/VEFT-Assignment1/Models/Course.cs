﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VEFT_Assignment1.Models
{
    /// <summary>
    /// Represents a course in a university
    /// </summary>
    public class Course
    {
        /// <summary>
        /// Name of the course
        /// Example: "Vefþjónustur"
        /// </summary>
        [Required]
        public String Name { get; set; }
        /// <summary>
        /// ID code for a given course
        /// Example: "T-514-VEFT"
        /// </summary>
        [Required]
        public String TemplateID { get; set; }
        /// <summary>
        /// Unique ID number for a given course
        /// Example: 1
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Start date of a given course
        /// Example: 2015-08-17
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of a given course
        /// Example: 2015-11-08
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// List of students attending a given course
        /// </summary>
        public List<Student> Students { get; set; }
    }
}