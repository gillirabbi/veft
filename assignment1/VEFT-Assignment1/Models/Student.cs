﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VEFT_Assignment1.Models
{
    /// <summary>
    /// Represents a student attending a course in a university
    /// </summary>
    public class Student
    {
        /// <summary>
        /// The social security number for a given student
        /// Example: 1602896969
        /// </summary>
        [Required]
        public String SSN { get; set; }
        /// <summary>
        /// The name of a given student
        /// Example: "Gunnar Gylfason"
        /// </summary>
        [Required]
        public String Name { get; set; }
    }
}