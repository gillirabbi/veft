﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using VEFT_Assignment1.Models;

namespace VEFT_Assignment1.Controllers
{
    [RoutePrefix("api/courses")]
    public class CourseController : ApiController
    {
        private static int _id = 1;
        private static List<Course> _courses;
        public CourseController()
        {
            if (_courses == null)
            {
                _courses = new List<Course>
                {
                    new Course
                    {
                        ID = _id++,
                        Name = "Web Services",
                        TemplateID = "T-512-VEFT",
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddMonths(3),
                        Students = new List<Student>
                        {
                            new Student
                            {
                                SSN = "1111119999",
                                Name = "Óskar Gunnar Fannars- og Guðrúnarson"
                            },
                            new Student
                            {
                                SSN = "1706448929",
                                Name = "Jón Sigurðarson"
                            }
                        }
                    },
                    new Course
                    {
                        ID = _id++,
                        Name = "Computer Networking",
                        TemplateID = "T-409-TSAM",
                        StartDate = DateTime.Now.AddDays(2),
                        EndDate = DateTime.Now.AddMonths(4),
                        Students = new List<Student>
                        {
                            new Student
                            {
                                SSN = "1203891259",
                                Name = "Ásdís Rán Baldursdóttir"
                            },
                            new Student
                            {
                                SSN = "1402901569",
                                Name = "Eric Fontana"
                            }
                        }
                    },
                    new Course {
                        ID = _id++,
                        Name = "Programming",
                        TemplateID = "T-111-PROG",
                        StartDate = DateTime.Now.AddYears(3),
                        EndDate = DateTime.Now.AddYears(5),
                        Students = new List<Student>
                        {
                            new Student
                            {
                                SSN = "0703854459",
                                Name = "Damian Pawlik"
                            },
                            new Student
                            {
                                SSN = "1411793249",
                                Name = "Súsanna Dóróteudóttir Möller"
                            }
                        }
                    }
                };
            }
        }

        /// <summary>
        /// Get info about a course
        /// </summary>
        /// <param name="id">The unique ID of the course</param>
        /// <returns>A course object</returns>
        [HttpGet]
        [Route("{id:int}", Name = "GetCourse")]
        public Course GetCourse(int id)
        {
            foreach (Course course in _courses)
            {
                if (course.ID == id)
                {
                    return course;
                }
            }
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Get a list of all courses
        /// </summary>
        /// <returns>A list of all courses</returns>
        [HttpGet]
        [Route("")]
        public List<Course> GetCourses()
        {
            return _courses;
        }

        /// <summary>
        /// Creates a new course
        /// Throws BadRequest exception if required fields are not filled
        /// </summary>
        /// <param name="model">A model containing all the new course data</param>
        /// <returns>A http response with the new course</returns>
        [HttpPost]
        [ResponseType(typeof(Course))]
        [Route("")]
        public IHttpActionResult PostCourse(Course model)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var course = model;
            course.ID = _id++;
            course.Students = new List<Student>();
            _courses.Add(course);
            var location = Url.Link("GetCourse", new { id = course.ID });
            return Created(location, course);
        }

        /// <summary>
        /// Add a student to a given course
        /// Throws BadRequest exception if required fields are not filled
        /// </summary>
        /// <param name="courseId">The id of the course that the student will be added to</param>
        /// <param name="model">A model containing all data for the student</param>
        /// <returns>A http response with the new course</returns>
        [HttpPost]
        [ResponseType(typeof(Student))]
        [Route("{courseId:int}/students")]
        public IHttpActionResult PostStudent(int courseId, Student model)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var student = model;
            var course = GetCourse(courseId);
            course.Students.Add(student);
            var location = Url.Link("GetStudents", new { id = courseId }); 
            return Created(location, student);
        }

        /// <summary>
        /// Delete a course from the courselist
        /// </summary>
        /// <param name="id">ID of course to be deleted</param>
        /// <returns>The course that was deleted</returns>
        [HttpDelete]
        [Route("{id:int}")]
        public Course DeleteCourse(int id)
        {
            Course course = GetCourse(id);
            _courses.Remove(course);
            return course;
        }

        /// <summary>
        /// Get a list of students for a given course
        /// </summary>
        /// <param name="id">Id of the course being checked</param>
        /// <returns>List of students in given course</returns>
        [HttpGet]
        [Route("{id:int}/students", Name = "GetStudents")]
        public List<Student> GetStudents(int id)
        {
            Course course = GetCourse(id);
            return course.Students;
        }

        /// <summary>
        /// Update data of a given course
        /// Throws BadRequest exception if all required data is not filled
        /// </summary>
        /// <param name="id">Id of course to be changed</param>
        /// <param name="model">A model with new data for the given course</param>
        /// <returns>The course with its new data</returns>
        [HttpPut]
        [Route("{id:int}")]
        public Course UpdateCourse(int id, Course model)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            Course course = GetCourse(id);
            course.Name = model.Name;
            course.TemplateID = model.TemplateID;
            course.StartDate = model.StartDate;
            course.EndDate = model.EndDate;
            return course;
        }
    }
}
