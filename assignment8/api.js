'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const models = require('./models');
const api = express();

api.use(bodyParser.json());

// Admin token
const ADMIN_TOKEN = "deadbeef-1337-1337-1337-deadbeef1337";

// Default users, uncomment below to instantiate
const user1 = new models.User({ name: "Gunnar Gylfason", age: 13, gender: "m" });
const user2 = new models.User({ name: "Bertel Benóný Bertelsson", age: 14, gender: "o" });
const user3 = new models.User({ name: "Edda Rúta Petursdóttir", age: 15, gender: "f" });

// user1.save((err, data) => {
// 	if (err) throw err;
// 	console.log("User created: " + data);
// });

// user2.save((err, data) => {
// 	if (err) throw err;
// 	console.log("User created: " + data);
// });

// user3.save((err, data) => {
// 	if (err) throw err;
// 	console.log("User created: " + data);
// });

// Get all companies
api.get('/company', (req, res) => {
	models.Company.find({}, (err, data) => {
		if (err) return res.status(500).json(err);
		res.json(data);
	});
});

// Get company with given id
api.get('/company/:id', (req, res) => {
	models.Company.findById(req.params.id, (err, data) => {
		if (err) return res.status(500).json(err);
		if (data == null) return res.status(404).end();
		res.json(data);
	});
});

// Add new company, user needs to have ADMIN_TOKEN correct
api.post('/company', (req, res) => {
	if (ADMIN_TOKEN != req.body.ADMIN_TOKEN) return res.status(401).end();
	const company = new models.Company(req.body);
	company.save((err, data) => {
		if (err) {
			if (err.name == 'ValidationError')
				return res.status(412).end();
			else
				return res.status(500).json(err);
		}
		res.status(201).json({ company_id: company._id });
	})
});

// Get all users
api.get('/user', (req, res) => {
	models.User.find({}, (err, data) => {
		if (err) return res.status(500).json(err);
		res.json(data);
	})
});

// Create a new punchcard
api.post('/punchcard/:companyid', (req, res) => {
	const token = req.body.TOKEN;
	let lifetime;

	const reg = new RegExp("^[0-9a-fA-F]{24}$");
	if (!reg.test(req.params.companyid)) {
		res.status(404).end();
		return;
	}

	// Check if company id is valid
	models.Company.findById(req.params.companyid, (err, data) => {
		if (err) return res.status(500).json(err);
		if (data == null) return res.status(404).end();
		lifetime = data.punchcard_lifetime;
	});


	// Check if a user has the given token
	models.User.findOne({ "token": token }, (err, data) => {
		if (res.headersSent)
			return;

		if (err) return res.status(500).json(err);
		if (data == null) return res.status(401).end();

		// Check if punchcard exists and isn't expired
		let now = new Date();
		models.Punchcard.findOne({ "company_id": req.params.companyid, "user_id": data._id }, (err, data2) => {
			if (err) return res.status(500).json(err);

			const punchcard = new models.Punchcard({
				company_id: req.params.companyid,
				user_id: data._id
			});

			if (data2 == null) {
				punchcard.save((err, data) => {
					if (err) return res.status(500).json(err);
					res.status(201).json({ punchcard_id: punchcard._id });
				});
			} else {
				let expire = new Date(data2.created);
				expire.setDate(expire.getDate() + lifetime);
				if (now.getTime() < expire.getTime()) {
					return res.status(409).end();
				} else {
					punchcard.save((err, data) => {
						if (err) return res.status(500).json(err);
						res.status(201).json({ punchcard_id: punchcard._id });
					});
				}
			}
		});
	});	
});

module.exports = api;