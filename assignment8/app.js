'use strict';
const express = require('express');
const api = require('./api');

const app = express();
const port = 1337;

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/app');

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Db connection error:'));
db.once('open', (callback) => {
	console.log('Connection to db successful');
});

app.use('/api', api);
app.listen(process.env.PORT || port, () => {
	console.log('Server successfully start on port ' + port);
});