'use strict';

const mongoose = require('mongoose');
const uuid = require('node-uuid');

const UserSchema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	token: {
		type: String,
		required: true,
		default: uuid.v4,
		select: false
	},
	age: {
		type: Number,
		required: true
	},
	gender: {
		type: String,
		required: true
	}
});

const CompanySchema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	punchcard_lifetime: {
		type: Number,
		required: true
	}
});

const PunchcardSchema = mongoose.Schema({
	company_id: {
		type: mongoose.Schema.ObjectId,
		required: true
	},
	user_id: {
		type: mongoose.Schema.ObjectId,
		required: true
	},
	created: {
		type: Date,
		required: true,
		default: Date.now
	}
});

module.exports = {
	User: mongoose.model('User', UserSchema),
	Company: mongoose.model('Company', CompanySchema),
	Punchcard: mongoose.model('Punchcard', PunchcardSchema)
};