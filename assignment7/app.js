'use strict';
let express = require('express');
let bodyParser = require('body-parser'); // Uses the body-parser middleware since it's no longer a part of the express core
let app = express();

app.use(bodyParser.json());


// Array of companies
let compid = 0;
let companies = [
  { id: compid++, name: "Te & Kaffi", punchCount: 10 },
  { id: compid++, name: "Kaffi & Te", punchCount: 100 },
  { id: compid++, name: "Kaffitár", punchCount: 5 },
  { id: compid++, name: "Hekla", punchCount: 5 },
  { id: compid++, name: "Byko", punchCount: 15 }
];


// Array of users
let userid = 0;
let users = [
	{ id: userid++, name: "Gísli Rafn Guðmundsson", email: "gisli13@ru.is" },
	{ id: userid++, name: "Gunnar Páll Gunnarsson", email: "gunnarpg13@ru.is" }
];

// Array of punches
let punches = [
	{ userid: 0, companyid: 0, dateCreated: new Date(98, 1) },
	{ userid: 0, companyid: 1, dateCreated: new Date(95, 1) }
];

// Returns a list of all companies in the system
app.get('/api/companies', (req, res) => {
	res.json(companies);
});

// Adds a new company to the system
app.post('/api/companies', (req, res) => {
	if (!req.body.hasOwnProperty('name') || !req.body.hasOwnProperty('punchCount')) {
		res.statusCode = 400;
		return res.send("Error 400: name and punchCount are required");
	}
	companies.push({ id: compid++, name: req.body.name, punchCount: req.body.punchCount });
	res.statusCode = 201;
	res.send("Company added successfully");
});

// Returns a company with a given id
app.get('/api/companies/:id', (req, res) => {
	let result = companies.filter((company) => {
		return company.id == req.params.id;
	});

	if (result.length > 0) {
		return res.json(result);
	} else {
		res.statusCode = 404;
		return res.send("Error 404: company not registered");
	}
});

// Returns a list of all users in the system
app.get('/api/users', (req, res) => {
	res.json(users);
});

// Adds a new user to the system
app.post('/api/users', (req, res) => {
	if (!req.body.hasOwnProperty('name') || !req.body.hasOwnProperty('email')) {
		res.statusCode = 400;
		return res.send("Error 400: name and email are required");
	}
	users.push({ id: userid++, name: req.body.name, email: req.body.email });
	res.statuscode = 201;
	req.send("User added successfully");
});

// Returns a list of all puches registered for a given user
// The optional parameter "?company=value" can be used to also filter the results by company id
app.get('/api/users/:id/punches', (req, res) => {
	if (!userExists(req.params.id)) {
		res.statusCode = 404;
		return res.send("Error 404: user is not registered");
	}

	let result = punches.filter((punch) => {
		return punch.userid == req.params.id;
	});

	if (req.query.company !== undefined) {
		result = result.filter((obj) => {
			return obj.companyid == req.query.company;
		});
	}

	let output = [];
	for (let punch in result) {
		output.push({ company: getCompanyName(result[punch].companyid), dateCreated: result[punch].dateCreated.toString() });
	}

	if (output.length > 0) {
		return res.json(output);
	} else {
		return res.status(204).send();
	}
});

// Adds new punch to a user with a given id
app.post('/api/users/:id/punches', (req, res) => {
	if (!userExists(req.params.id)) {
		res.statusCode = 404;
		return res.send("Error 404: user is not registered");
	}

	if (!req.body.hasOwnProperty('companyId')) {
		res.statusCode = 400;
		return res.send("Error 400: company id is required");
	}

	punches.push({ userid: req.params.id, companyid: req.body.companyId, dateCreated: new Date(Date.now()) })
	res.statusCode = 201;
	res.send("Punch added successfully");
});

// Helpers

// Returns the company name for a given id
function getCompanyName(id) {
	let result = companies.filter((company) => {
		return company.id == id;
	});
	return result[0].name;
}

// Checks if the user exists for a given id
function userExists(id) {
	if (users.filter((user) => { return user.id == id; }).length > 0) {
		return true;
	}
	return false;
}


app.listen(process.env.PORT || 1337);