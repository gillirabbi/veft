'use strict';
const express = require('express');
const api = require('./api');

const app = express();
const port = 4000;

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/assignment10');

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Db connection error:'));
db.once('open', (callback) => {
	console.log('Connection to db successful');
});

app.use('', api);
app.listen(process.env.PORT || port, () => {
	console.log('Server successfully started on port ' + port);
});
