'use strict';

const mongoose = require('mongoose');

const CompanySchema = mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	url: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		required: true,
		default: Date.now
	}

});

module.exports = {
	Company: mongoose.model('Company', CompanySchema)
};