'use strict';
const express = require('express');
const bodyparser = require('body-parser');
const elasticsearch = require('elasticsearch');

const models = require('./models');
const api = express();
const client = new elasticsearch.Client({
	host: 'localhost:9200',
	log: 'error'
});

// Check for ElasticSearch connection
client.ping({
  requestTimeout: Infinity,
  hello: "elasticsearch!"
}, (error) => {
  if (error) {
    console.trace('ElasticSearch cluster is down!');
  } else {
    console.log('ElasticSearch cluster is up and reachable');
  }
});

api.use(bodyparser.json());

// Admin token
// Simplified not to use uuid
const ADMIN_TOKEN = "admin";

// Create new company
api.post('/companies', (req, res) => {
	if (req.headers['admin_token'] !== ADMIN_TOKEN) 
		return res.status(401).end();
	if (req.headers['content-type'] !== 'application/json')
		return res.status(415).end();

	const company = new models.Company(req.body);
	models.Company.find({ "title": company.title }, (err, data) => {
		if (err)
			return res.status(500).json(err);
		if (data.length)
			return res.status(409).end();

		company.save((err, data) => {
			if (err) {
				if (err.name === 'ValidationError')
					return res.status(412).end();
				else
					return res.status(500).json(err);
			}

			const promise = client.index({
				index: 'companies',
				type: 'company',
				id: company._id.toString(),
				body: {
					id: company._id.toString(),
					title: company.title,
					description: company.description,
					url: company.url
				}
			});

			promise.then((data) => {
				res.status(201).json({ id: company._id });
			}, (err) => {
				res.status(500).end(err);
			});
		});
	});
});

// Get all companies using ElasticSearch, sorting is still wonky
api.get('/companies', (req, res) => {
	const page = req.query.page || 0;
	const max = req.query.max || 20;
	
	const promise = client.search({
		index: 'companies',
		type: 'company',
		body: {
			from: page * max,
			size: max,
			sort: [
				{"title": {"order": "desc"}}
			]
		}
	});

	promise.then((data) => {
		res.status(200).json(data.hits.hits.map((x) => x._source));
	}, (err) => {
		res.status(500).send(err);
	});
});

// Get company by id using MongoDB
api.get('/companies/:id', (req, res) => {
	// Check if id is valid using RegEx
	const reg = new RegExp("^[0-9a-fA-F]{24}$");
	if (!reg.test(req.params.id))
		return res.status(404).end();

	models.Company.findById(req.params.id, (err, data) => {
		if (err)
			return res.status(500).send(err);
		if (data == null)
			return res.status(404).end();
		return res.status(200).json({
			id: data._id,
			title: data.title,
			description: data.description,
			url: data.url
		});
	});
});

// Search for companies
api.post('/companies/search', (req, res) => {
	const search = req.body.search || '';
	const promise = client.search({
		index: 'companies',
		type: 'company',
		body: {
			query: {
				query_string: {
					query: search
				}
			}
		}
	});

	promise.then((data) => {
		if (!data.hits.hits.length)
			return res.status(204).end();
		return res.status(200).json(data.hits.hits.map((x) => x._source));
	}, (err) => {
		return res.status(500).send(err);
	});
});

// Update company with given id
api.post('/companies/:id', (req, res) => {
	if (req.headers['admin_token'] !== ADMIN_TOKEN) 
		return res.status(401).end();
	if (req.headers['content-type'] !== 'application/json')
		return res.status(415).end();

	// Check if id is valid using RegEx
	const reg = new RegExp("^[0-9a-fA-F]{24}$");
	if (!reg.test(req.params.id))
		return res.status(404).end();

	models.Company.findById(req.params.id, (err, data) => {
		if (err)
			return res.status(500).send(err);
		if (data == null)
			return res.status(404).end();

		data.title = req.body.title || data.title;
		data.description = req.body.description || data.description;
		data.url = req.body.url || data.url;

		data.save((err, data) => {
			if (err)
				return res.status(500).send(err);

			const promise = client.index({
				index: 'companies',
				type: 'company',
				id: data._id.toString(),
				body: {
					title: data.title,
					description: data.description,
					data: data.url
				}
			});

			promise.then((esdata) => {
				return res.status(200).send({
					id: data._id,
					title: data.title,
					description: data.description,
					url: data.url
				});
			}, (err) => {
				return res.status(500).send(err);
			});
		});
	});
});

// Delete a company by id
api.delete('/companies/:id', (req, res) => {
	if (req.headers['admin_token'] !== ADMIN_TOKEN) 
		return res.status(401).end();
	if (req.headers['content-type'] !== 'application/json')
		return res.status(415).end();

	// Check if id is valid using RegEx
	const reg = new RegExp("^[0-9a-fA-F]{24}$");
	if (!reg.test(req.params.id))
		return res.status(404).end();

	models.Company.findByIdAndRemove(req.params.id, (err, data) => {
		if (err)
			return res.status(500).send(err);
		if (data == null)
			return res.status(404).end();

		const promise = client.delete({
			index: 'companies',
			type: 'company',
			id: data._id.toString(),
		});

		promise.then((esdata) => {
			return res.status(204).end();
		}, (err) => {
			return res.status(500).send(err);
		});
	});
});

module.exports = api;