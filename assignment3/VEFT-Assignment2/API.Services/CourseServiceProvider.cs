﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services.Entities;
using API.Services.Repositories;

namespace API.Services
{
    /// <summary>
    /// Service provider class for Courses
    /// A collection of service calls used by controllers
    /// </summary>
    public class CourseServiceProvider
    {
        private readonly AppDataContext _db;

        /// <summary>
        /// Initializes database context when created
        /// </summary>
        public CourseServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// A helper function for getting a course with a given id
        /// It is never used in a controller, since GetCourseDetailsById has more relevant info
        /// </summary>
        /// <param name="id">Id of the course being looked at</param>
        /// <returns>A CourseDTO with the given course id</returns>
        private CourseDTO GetCourseById(int id)
        {
            var result = (from c in _db.Courses
                          from d in _db.CourseTemplates
                          where c.TemplateId == d.TemplateId
                          where c.Id == id
                          select new CourseDTO
                          {
                              Id = c.Id,
                              Name = d.Name,
                              StartDate = c.StartDate,
                              EndDate = c.EndDate,
                              Semester = c.Semester,
                              MaxStudents = c.MaxStudents
                          }).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Creates a new course
        /// </summary>
        /// <param name="model">A viewmodel containing all data to be inserted</param>
        /// <returns>Returns the new course back to the user</returns>
        public CourseDTO PostCourse(CourseViewModel model)
        {
            var course = new Course
            {
                TemplateId = model.TemplateId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Semester = model.Semester,
                MaxStudents = model.MaxStudents
            };
            _db.Courses.Add(course);
            _db.SaveChanges();

            return GetCourseById(course.Id);
        }

        /// <summary>
        /// Gets all courses registered on the given semester
        /// </summary>
        /// <param name="semester">The semester being searched</param>
        /// <returns>A list of courses in the CourseDTO format</returns>
        public List<CourseDTO> GetCoursesBySemester(string semester = null)
        {
            if (string.IsNullOrEmpty(semester))
            {
                semester = "20153";
            }

            var result = (from c in _db.Courses
                from d in _db.CourseTemplates
                where c.TemplateId == d.TemplateId
                where c.Semester == semester
                select new CourseDTO
                {
                    Id = c.Id,
                    Name = d.Name,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate,
                    Semester = c.Semester,
                    MaxStudents = c.MaxStudents,
                    StudentCount = (from cs in _db.CourseStudents
                                    where cs.CourseId == c.Id
                                    select cs).Count()
                }).ToList();

            return result;
        }

        /// <summary>
        /// Retreives details about a course with the given id
        /// </summary>
        /// <param name="id">Id of the course being checkout out</param>
        /// <returns>A CourseDetailsDTO object with all the relevant data</returns>
        public CourseDetailsDTO GetCourseDetailsById(int id)
        {
            var result = (from c in _db.Courses
                from d in _db.CourseTemplates
                where c.TemplateId == d.TemplateId
                where c.Id == id
                select new CourseDetailsDTO
                {
                    Id = c.Id,
                    Name = d.Name,
                    MaxStudents = c.MaxStudents
                }).FirstOrDefault();
            if (result == null)
                return null;
            var students = (from s in _db.Students
                from cs in _db.CourseStudents
                where cs.CourseId == id
                where cs.StudentId == s.Id
                select new StudentDTO
                {
                    Name = s.Name,
                    SSN = s.SSN
                }).ToList();
            result.Students = students;

            return result;
        }

        /// <summary>
        /// Alter a courses start and/or end date
        /// </summary>
        /// <param name="id">Id of course being altered</param>
        /// <param name="model">Model with start and end date, irrelevant data discarded</param>
        /// <returns>The newly altered object</returns>
        public CourseDTO PutCourseById(int id, CourseViewModel model)
        {
            var result = (from c in _db.Courses
                where c.Id == id
                select c).FirstOrDefault();
            if (result == null)
                return null;
            result.StartDate = model.StartDate;
            result.EndDate = model.EndDate;
            _db.SaveChanges();
            return GetCourseById(id);
        }

        /// <summary>
        /// Removes a course from the database
        /// </summary>
        /// <param name="id">Id of course being removed</param>
        /// <returns>The course item that was just removed</returns>
        public CourseDTO DeleteCourseById(int id)
        {
            var result = GetCourseById(id);
            var delete = (from c in _db.Courses
                where c.Id == id
                select c).FirstOrDefault();
            if (delete == null)
                return null;
            _db.Courses.Remove(delete);
            _db.SaveChanges();
            return result;
        }
    }
}
