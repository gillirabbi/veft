﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Services.Entities
{
    /// <summary>
    /// Waiting list containing students that could not register for a course
    /// </summary>
    class Waitinglist
    {
        /// <summary>
        /// Id of student-course relation
        /// Example: 1
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Id of student on waiting list
        /// Example: 1
        /// </summary>
        public int StudentId { get; set; }
        /// <summary>
        /// Id of course the given student is on waiting list for
        /// Example: 1
        /// </summary>
        public int CourseId { get; set; }
    }
}
