﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services.Entities;
using API.Services.Repositories;

namespace API.Services
{
    /// <summary>
    /// Service provider class for Students
    /// </summary>
    public class StudentServiceProvider
    {
        private readonly AppDataContext _db;

        /// <summary>
        /// Initializes database context when created
        /// </summary>
        public StudentServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// Gets all students registered in the given course
        /// </summary>
        /// <param name="courseId">Id of course being fetched from</param>
        /// <returns>A list of students in the given course</returns>
        public List<StudentDTO> GetStudentsByCourse(int courseId)
        {
            var result = (from s in _db.Students
                from cs in _db.CourseStudents
                where s.Id == cs.StudentId
                where cs.CourseId == courseId
                select new StudentDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                    SSN = s.SSN
                }).ToList();
            return result;
        }

        /// <summary>
        /// Adds a new student to the given course
        /// </summary>
        /// <param name="courseId">Id of course being added to</param>
        /// <param name="model">Viewmodel with the new students info</param>
        /// <returns>An object with the new students data</returns>
        public StudentDTO PostStudentToCourse(int courseId, StudentViewModel model)
        {
            var result = (from s in _db.Students
                where s.SSN == model.SSN
                select s).FirstOrDefault();
            if (result == null)
                return null;
            _db.CourseStudents.Add(new CourseStudent { CourseId = courseId, StudentId = result.Id });
            _db.SaveChanges();

            return new StudentDTO { Id = result.Id, Name = result.Name, SSN = result.SSN };
        }

        /// <summary>
        /// Removes given student from given course
        /// Does not throw error if deletion does not succeed
        /// </summary>
        /// <param name="courseId">Id of the course</param>
        /// <param name="ssn">SSN of the student</param>
        public void DeleteStudentFromCourse(int courseId, string ssn)
        {
            var result = (from s in _db.Students
                from cs in _db.CourseStudents
                where cs.StudentId == s.Id
                where cs.CourseId == courseId
                where s.SSN == ssn
                select cs).FirstOrDefault();
            _db.CourseStudents.Remove(result);
            _db.SaveChanges();
        }

        /// <summary>
        /// Gets the student with the id given
        /// </summary>
        /// <param name="id">Id of student</param>
        /// <returns>Student object</returns>
        public StudentDTO GetStudentById(int id)
        {
            var result = (from s in _db.Students
                where s.Id == id
                select s).FirstOrDefault();
            return result == null ? null : new StudentDTO { Id = result.Id, Name = result.Name, SSN = result.SSN };
        }

        /// <summary>
        /// Checks if the student is already registered to the given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="SSN">Id of student</param>
        /// <returns>True if registered, otherwise false</returns>
        public bool Registered(int courseId, string SSN)
        {
            var result = (from s in _db.Students
                from cs in _db.CourseStudents
                where s.Id == cs.StudentId
                where s.SSN == SSN
                where cs.CourseId == courseId
                select s).Count();
            return result > 0;
        }

        /// <summary>
        /// Checks if course is full
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <returns>True if course is full, otherwise false</returns>
        public bool MaxReached(int courseId)
        {
            var count = (from c in _db.Courses
                where c.Id == courseId
                select c.MaxStudents).First();
            var students = (from cs in _db.CourseStudents
                where cs.CourseId == courseId
                select cs).Count();
            return students >= count;
        }

        /// <summary>
        /// Removes student from waiting list of given course
        /// Does not throw error if operation is not successful
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="ssn">SSN of student</param>
        public void RemoveFromWaitinglist(int courseId, string ssn)
        {
            var result = (from s in _db.Students
                from w in _db.Waitinglists
                where s.Id == w.StudentId
                where s.SSN == ssn
                where w.CourseId == courseId
                select w).FirstOrDefault();
            if (result == null)
                return;
            _db.Waitinglists.Remove(result);
            _db.SaveChanges();
        }
    }
}
