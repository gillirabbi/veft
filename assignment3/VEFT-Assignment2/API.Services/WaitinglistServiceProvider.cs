﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services.Entities;
using API.Services.Repositories;

namespace API.Services
{
    /// <summary>
    /// Service provider for the waiting list controller
    /// </summary>
    public class WaitinglistServiceProvider
    {
        private readonly AppDataContext _db;

        /// <summary>
        /// Initializes database context when created
        /// </summary>
        public WaitinglistServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// Puts student on waiting list for given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="model">Model that contains student info</param>
        /// <returns>Student object</returns>
        public StudentDTO PostToWaitinglist(int courseId, StudentViewModel model)
        {
            var result = (from s in _db.Students
                where s.SSN == model.SSN
                select s).FirstOrDefault();
            if (result == null)
                return null;
            _db.Waitinglists.Add(new Waitinglist { StudentId = result.Id, CourseId = courseId });
            _db.SaveChanges();
            return new StudentDTO { Id = result.Id, Name = result.Name, SSN = result.SSN };
        }

        /// <summary>
        /// Gets a list of students on the waiting list of given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <returns>List of students</returns>
        public List<StudentDTO> GetStudentsFromWaitinglist(int courseId)
        {
            var result = (from s in _db.Students
                from w in _db.Waitinglists
                where w.StudentId == s.Id
                where w.CourseId == courseId
                select new StudentDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                    SSN = s.SSN
                }).ToList();
            return result;
        } 

        /// <summary>
        /// Checks if student is already on the given course's waiting list
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="model">Model containing info on student</param>
        /// <returns>True if already on waiting list, otherwise false</returns>
        public bool OnWaitinglist(int courseId, StudentViewModel model)
        {
            var result = (from s in _db.Students
                from w in _db.Waitinglists
                where w.StudentId == s.Id
                where w.CourseId == courseId
                where s.SSN == model.SSN
                select w).Count();
            return result > 0;
        }

        /// <summary>
        /// Checks if student is already enrolled in given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="model">Model containing info on student</param>
        /// <returns>True if student is enrolled, otherwise false</returns>
        public bool Enrolled(int courseId, StudentViewModel model)
        {
            var result = (from s in _db.Students
                from cs in _db.CourseStudents
                where cs.StudentId == s.Id
                where cs.CourseId == courseId
                where s.SSN == model.SSN
                select cs).Count();
            return result > 0;
        }
    }
}
