using System;

namespace API.Models.DTO
{
    /// <summary>
    /// This class represents an item in a list of courses
    /// </summary>
    public class CourseDTO
    {
        /// <summary>
        /// Unique ID of the course
        /// Example: 1
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the course
        /// Example: "Vef�j�nustur"
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The semester in which the course is taught
        /// Example: "20153"
        /// </summary>
        public string Semester { get; set; }
        /// <summary>
        /// Start date of a given course
        /// Example: 2015-17-08
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of a given course
        /// Example: 2015-11-08
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Number of students registered in the given course
        /// Example: 30
        /// </summary>
        public int StudentCount { get; set; }
        /// <summary>
        /// Number of students able to register for a course
        /// Example: 10
        /// </summary>
        public int MaxStudents { get; set; }
    }
}
