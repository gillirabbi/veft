using System.Collections.Generic;

namespace API.Models.DTO
{
    /// <summary>
    /// This class represents a single course and contains various details about the course
    /// </summary>
    public class CourseDetailsDTO
    {
        /// <summary>
        /// Unique ID for a given course
        /// Example: 1
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the course
        /// Example: "Vef�j�nustur"
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Number of students able to register to the course
        /// Example: 10
        /// </summary>
        public int MaxStudents { get; set; }
        /// <summary>
        /// A list of students attending the course
        /// </summary>
        public List<StudentDTO> Students { get; set; }
    }
}
