﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services;

namespace VEFT_Assignment2.Controllers
{
    /// <summary>
    /// Controller for waiting list
    /// </summary>
    [RoutePrefix("api/courses/{courseId}/waitinglist")]
    public class WaitinglistController : ApiController
    {
        private readonly WaitinglistServiceProvider _service;

        /// <summary>
        /// Initializes a waiting list service provider which contains all business logic
        /// </summary>
        public WaitinglistController()
        {
            _service = new WaitinglistServiceProvider();
        }

        /// <summary>
        /// Puts student on waiting list for given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <param name="model">Model containing info on student</param>
        /// <returns>Statuscode 200 on success, otherwise throws exception</returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostWaitinglist(int courseId, StudentViewModel model)
        {
            if (_service.OnWaitinglist(courseId, model) || _service.Enrolled(courseId, model))
                throw new HttpResponseException(HttpStatusCode.PreconditionFailed);
            var result = _service.PostToWaitinglist(courseId, model);
            if (result == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return StatusCode(HttpStatusCode.OK);
        }

        /// <summary>
        /// Gets a list of students on waiting list for given course
        /// </summary>
        /// <param name="courseId">Id of course</param>
        /// <returns>List of student objects</returns>
        [HttpGet]
        [Route("")]
        public List<StudentDTO> GetWaitinglist(int courseId)
        {
            return _service.GetStudentsFromWaitinglist(courseId);
        }
    }
}
