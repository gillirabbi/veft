﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models.DTO;
using API.Models.ViewModels;
using API.Services;

namespace VEFT_Assignment3.Controllers
{
    /// <summary>
    /// Controller for Courses
    /// </summary>
    [RoutePrefix("api/courses")]
    public class CoursesController : ApiController
    {
        private readonly CourseServiceProvider _service;

        /// <summary>
        /// Initializes a course service provider which contains all business logic
        /// </summary>
        public CoursesController()
        {
            _service = new CourseServiceProvider();
        }

        /// <summary>
        /// Get all courses for the given semester
        /// </summary>
        /// <param name="semester">Semester being checked</param>
        /// <returns>List of courses on the given semester</returns>
        [HttpGet]
        [Route("")]
        public List<CourseDTO> GetCourses(string semester = null)
        {
            return _service.GetCoursesBySemester(semester);
        }

        /// <summary>
        /// Details about the course with the given id
        /// </summary>
        /// <param name="id">Id of course being checked</param>
        /// <returns>Detailed object for the given course</returns>
        [HttpGet]
        [Route("{id}", Name="GetCourse")]
        public CourseDetailsDTO GetCourse(int id)
        {
            var result = _service.GetCourseDetailsById(id);
            if (result == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return result;
        }

        /// <summary>
        /// Create a new course
        /// </summary>
        /// <param name="model">Data course being created</param>
        /// <returns>An object with the newly created course's data</returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostCourse(CourseViewModel model)
        {
            var course = _service.PostCourse(model);
            var location = Url.Link("GetCourse", new { id = course.Id });
            return Created(location, course);
        }

        /// <summary>
        /// Change start and/or end date of given course
        /// </summary>
        /// <param name="id">Id of course being changed</param>
        /// <param name="model">New start/end date for course</param>
        /// <returns>The course with it's new data</returns>
        [HttpPut]
        [Route("{id}")]
        public CourseDTO PutCourse(int id, CourseViewModel model)
        {
            var result = _service.PutCourseById(id, model);
            if (result == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return result;
        }

        /// <summary>
        /// Remove the given course
        /// </summary>
        /// <param name="id">Id of course</param>
        /// <returns>Object with data of the course that was just deleted</returns>
        [HttpDelete]
        [Route("{id}")]
        public CourseDTO DeleteCourse(int id)
        {
            var result = _service.DeleteCourseById(id);
            if (result == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return result;
        }
    }
}
