# README #

# T-514-VEFT - Vefþjónustur #
## Reykjavík University - Fall 2015 ##

### Description ###

Repository containing VEFT projects - server-side web programming with emphasis on:

- ASP.NET Web API/C#
- Node.js / JavaScript
- REST
- etc.