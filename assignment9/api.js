'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const models = require('./models');
const producer = require('./producer');
const consumer = require('./consumer');
const api = express();

api.use(bodyParser.json());

// Add new user
api.post('/user', (req, res) => {
	if (!req.body.hasOwnProperty('username') || !req.body.hasOwnProperty('email'))
		return res.status(412).send("Username and email are required");

	const user = new models.User(req.body);
	models.User.find({ $or: [{ "username": user.username}, { "email": user.email }]}, (err, data) => {
		if (err) return res.status(500).json(err);
		if (data.length > 0) return res.status(409).send("Username or email is already taken.");
		user.save((err, data) => {
			if (err)  {
				if (err.name == 'ValidationError')
					return res.status(412).send("Post object not valid");
				else
					return res.status(500).json(err);
			}
			res.status(201).json(data);
			producer.sendMessage(user);
		});
	});
});

// Get token for user
api.post('/token', (req, res) => {
	if (!req.body.hasOwnProperty('username') || !req.body.hasOwnProperty('password'))
		return res.status(412).send("Username and password are required");

	models.User.find({ 'username': req.body.username, 'password': req.body.password }, (err, data) => {
		if (err) return res.status(500).json(err);
		if (data.length != 1) return res.status(401).send("Wrong username or password");
		if (data[0].token === undefined) return res.status(500).send("Token unavailable");
		res.status(200).json({ token: data[0].token });
	});
});

module.exports = api;