'use strict';
const express = require('express');
const kafka = require('kafka-node');
const app = express();

const HighLevelProducer = kafka.HighLevelProducer;
const client = new kafka.Client('localhost:2181');
const producer = new HighLevelProducer(client);

module.exports = {
	// Sends kafka message to queue
	sendMessage: (message) => {
		const data = [{
			topic: 'users',
			messages: JSON.stringify(message)
		}];
		producer.send(data, (err, data) => {
			if (err) { 
				console.log('Error: ', err);
				return;
			}
			console.log('Kafka message sent: ' + data);
		});
	}
};

producer.on('ready', () => {
	console.log('Kafka producer is ready');
});