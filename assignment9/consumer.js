'use strict';
const kafka = require('kafka-node');
const uuid = require('node-uuid');
const mongoose = require('mongoose');
const models = require('./models');

const HighLevelConsumer = kafka.HighLevelConsumer;
const client = new kafka.Client('localhost:2181');
const consumer = new HighLevelConsumer(
	client, 
	[{ topic: 'users' }],
	{
		groupId: 'mygroup'
	}
);

// Generate token for user when message is received
consumer.on('message', (message) => {
	const value = JSON.parse(message.value);

	models.User.findByIdAndUpdate(value._id, { $set: { 'token': uuid.v4() }}, { new: true }, (err, data) => {
		if (err) console.log('Error: ' + err);
		console.log("Token generated for: " + data.id);
	}); 
}); 