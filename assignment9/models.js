'use strict';

const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	age: {
		type: Number,
		required: true
	},
	token: {
		type: String
	}
});

module.exports = {
	User: mongoose.model('User', UserSchema)
};